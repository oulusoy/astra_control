<?php
    require_once("includes/config.php");
    require_once("includes/mysql.php");

    $json_request = json_decode(file_get_contents('php://input'), true);

    //file_put_contents("/tmp/dump",$json_request,FILE_APPEND);

    $query = new db_query();

    $input_info = array_pop($json_request);		

    $input_info['scrambled']=$input_info['scrambled']?'true':'false';
    $input_info['onair']=$input_info['onair']?'true':'false';

    if (isset($input_info['channel_id'])) {
        $ret = $query->result("update input set
            scrambled = '".$query->escape($input_info['scrambled'])."',
            cc_error = '".$query->escape($input_info['cc_error'])."',
            pes_error = '".$query->escape($input_info['pes_error'])."',
            bitrate = '".$query->escape($input_info['bitrate'])."',
            onair = '".$query->escape($input_info['onair'])."',
            last_update = now()
            where channel_id = '".$query->escape($input_info['channel_id'])."'
            and input_id = '".$query->escape($input_info['input_id'])."'");
    }

    if (isset($input_info['dvb_id'])) {
        $dvb_id = preg_replace("/[^0-9]/", '', $query->escape($input_info['dvb_id']));
        $ret_dvb = $query->result("UPDATE `dvb_input` set
            `last_update` = FROM_UNIXTIME('".$query->escape($input_info['timestamp'])."'),
            `status` =  '".$query->escape($input_info['status'])."',
            `signal` =  '".$query->escape($input_info['signal'])."',
            `snr` =  '".$query->escape($input_info['snr'])."',
            `ber` =  '".$query->escape($input_info['ber'])."',
            `unc` =  '".$query->escape($input_info['unc'])."' WHERE  `dvb_input_id` =".$dvb_id.";");
    }

    //Сообщения через xmpp/jabber
    if (defined('XMPPLOGIN')) {
        require_once("includes/XMPPHP/XMPP.php");
    }
    require_once("includes/xmpp_alerts.php");                

?>

